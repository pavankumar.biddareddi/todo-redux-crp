import React from 'react';


const Todos =  (state = [], action ) => {

    switch(action.type){
        case 'ADD_TODO':
        return state.concat({
            value: action.value,
            status: action.status,
            id: action.id,
        });

        case 'DELETE_TODO':
        return state.filter(value =>
            value.id != action.id );

        case 'TOGGLE_TODO':
        
        return state.map( (value,key) => (
            (action.id == value.id)?{...value, status:!value.status}:value 
        ));;

        default:
        return state;
    }

}

export default Todos;