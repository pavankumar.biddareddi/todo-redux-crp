import React from 'react';
import { combineReducers } from 'redux';
import Todos from './Todos';
import VisiblityFilter from './Visibilityfilter';

export default combineReducers({
    Todos,
    VisiblityFilter
})