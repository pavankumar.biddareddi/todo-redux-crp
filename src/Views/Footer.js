import React from 'react';
import Footerlinks from '../Components/Footerlinks';

const Footer = () => {
    return (
        <div id="footer">
            <Footerlinks filter="all">
                All
            </Footerlinks>
            <Footerlinks filter="completed">
                Completed
            </Footerlinks>
            <Footerlinks filter="active">
                Active
            </Footerlinks>
        </div>
    );
}

export default Footer;