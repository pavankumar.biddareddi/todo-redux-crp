import React from 'react';
import ListItem from './Listitem';
import {List} from 'semantic-ui-react';

const ListTodos = ({todos, toggleTodo, handleRemove}) => {
    return (
        <div>
            <List divided verticalAlign='middle'>
                {
                    todos.map((v,k) => (
                        <ListItem {...v} key ={k}
                        onClick = { ()=> toggleTodo(v.id) }
                        handleRemove = { ()=>handleRemove(v.id) }
                        />
                    ))
                }
                {
                    (todos.length == 0) && (<li>No Items Found.</li>)
                }
            </List>
        </div>
    );  
}

export default ListTodos;