import React from 'react';
import {addTodo} from '../Actions';
import {connect} from 'react-redux';
import {Input} from 'semantic-ui-react';

const Field = ({dispatch}) => {
    // let input;

    return(
        <div>
            <form onSubmit={ (e)=>
                {
                    e.preventDefault();
                    console.log("this.input", this.input.inputRef.value);
                    if (this.input.inputRef.value.trim() === "") return '' ;
                    dispatch(addTodo(this.input.inputRef.value));
                    this.input.inputRef.value = "";
                    } }>
            <Input action="Add Todo" ref={ref => (this.input = ref) } placeholder="Enter Todo"/>
            </form>
        </div>
    );
}

export default connect()(Field);