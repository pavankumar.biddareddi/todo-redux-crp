import React from 'react';
import {List, Button} from 'semantic-ui-react';

const ListItem = (props) => {
return (
    <List.Item style={{ textDecoration: !props.status?'line-through':'' }}  
    
    >
    <List.Content>
    <span onClick= { ()=>props.onClick(props.id) }>{props.value}</span>
    </List.Content>
    <List.Content floated='right'>
    <Button size='mini' onClick = { () => props.handleRemove(props.id) }>X</Button>
    </List.Content>
    </List.Item>
);
}

export default ListItem;