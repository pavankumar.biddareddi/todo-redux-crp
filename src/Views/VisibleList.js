import React from 'react';
import {connect} from 'react-redux';
import ListTodos from './List';
import {toggleTodo, handleRemove} from '../Actions';

const getVisibleTodos = (todos, filter) => {
    switch(filter){
        case 'all':
            return todos;
        case "active":
            return todos.filter(t => (t.status));  
        case "completed":
            return todos.filter(t => (!t.status)); 
        default:
            return todos;
    }
}


const mapStateToProps = state => ({
    todos: getVisibleTodos(state.Todos, state.VisiblityFilter)
})
const mapDispatchToProps = dispatch => ({
    toggleTodo: id=> dispatch(toggleTodo(id)),
    handleRemove: id=> dispatch(handleRemove(id))
})

export default connect( mapStateToProps, mapDispatchToProps )(ListTodos);