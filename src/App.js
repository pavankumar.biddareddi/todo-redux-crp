import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';

import Field from './Views/Field.js';
import ListTodos from './Views/List';
import VisibleList from './Views/VisibleList';
import Footer from './Views/Footer';
import { Container, Header } from 'semantic-ui-react';


class App extends Component {
  render() {
    return (
      <Container>
        <Header as="h1" textAlign='center'>ToDo's</Header>
        <Field />
        <Container style={{ height:'300px', overflow:"auto"  }}>
          <VisibleList />
        </Container>
        <Footer />
      </Container>
    );
  }
}

export default App;
