import React from 'react';
import {Button} from 'semantic-ui-react';

const Link = ({ active, children, onClick  }) => {
    return (
        <Button 
        onClick= {onClick}
        disabled = {active}
        >
        {children}
        </Button>
    );
}

export default Link;