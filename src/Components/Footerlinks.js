import React from 'react';
import Link from './Link';
import {connect} from 'react-redux';
import {setVisibilityFilter} from '../Actions';



const mapStateToProps = (state, ownProps) => ({
    active: ownProps.filter === state.visiblefilter
})

const mapDispatchToProps = (dispatch, ownProps) => ({
    onClick: ()=> dispatch(setVisibilityFilter(ownProps.filter))
})

export default connect(mapStateToProps, mapDispatchToProps )(Link);